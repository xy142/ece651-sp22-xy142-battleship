package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
    @Test
    public void test_makeCoords(){
        HashSet<Coordinate> actual = RectangleShip.makeCoords(new Coordinate(0,0), 1, 3);
        HashSet<Coordinate> expected = new HashSet<Coordinate>();
        expected.add(new Coordinate(0,0));
        expected.add(new Coordinate(1,0));
        expected.add(new Coordinate(2,0));
        assertEquals(expected, actual);
    }

    @Test
    public void test_construct(){
        Ship<Character> s = new RectangleShip<Character>(new Coordinate(1,1), 3, 1,'s','*',"submarine");
        ShipDisplayInfo<Character> disp = new SimpleShipDisplayInfo<Character>('s', '*');
        // for convience and without loss of aim, enemyDI
        Ship<Character> s2 = new RectangleShip<>(new Coordinate(1,1), 3, 1, disp,"submarine",disp);
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,1)));
        assertEquals(true, s2.occupiesCoordinates(new Coordinate(1,2)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,3)));
        assertEquals(false, s.occupiesCoordinates(new Coordinate(3,0)));
        assertEquals(false, s.occupiesCoordinates(new Coordinate(0,1)));
        assertEquals("submarine", s.getName());

    }

    @Test
    public void test_recordHit_wasHitAt(){
        Ship<Character> s = new RectangleShip<Character>(new Coordinate(1,1), 3, 1, 's','*',"submarine");
        assertEquals(false, s.wasHitAt(new Coordinate(1,2),true));
        Coordinate invalid = new Coordinate(4,4);
        assertThrows(IllegalArgumentException.class,()->{s.wasHitAt(invalid,true);});
        assertEquals('s',s.getDisplayInfoAt(new Coordinate(1,2),true) );
        assertEquals(null,s.getDisplayInfoAt(new Coordinate(1,2),false));
        s.recordHitAt(new Coordinate(1,2));
        assertEquals(true, s.wasHitAt(new Coordinate(1,2),true));      
        assertEquals(false, s.isSunk());
        assertEquals('*',s.getDisplayInfoAt(new Coordinate(1,2),true) );
        assertEquals('s',s.getDisplayInfoAt(new Coordinate(1,2),false) );
        s.recordHitAt(new Coordinate(1,1));
        assertEquals(false, s.isSunk());
        s.recordHitAt(new Coordinate(1,3));
        assertEquals(true, s.isSunk());
    }
    /**
     * pass 
     */
    @Test
    public void test_recordHitV2(){
        Ship<Character> s = new RectangleShip<Character>(new Coordinate(1,1), 3, 1, 's','*',"submarine");
        ArrayList<Coordinate>  rel = new ArrayList<>();
        rel.add(new Coordinate(8,8));
        s.setRelativeHits(rel);
        assertEquals(rel, s.getRelativeHits());
    }

    @Test
    public void test_two_version_display_what_at(){
        Ship<Character> s = new RectangleShip<Character>(new Coordinate(1,1), 3, 1, 's','*',"submarine");
        ArrayList<Coordinate>  rel = new ArrayList<>();
        rel.add(new Coordinate(0,2));
        s.setRelativeHits(rel);
        assertEquals(rel, s.getRelativeHits());
        assertEquals(null, s.getDisplayInfoAt(new Coordinate(1,3), false));
        assertEquals('*', s.getDisplayInfoAt(new Coordinate(1,3), true));




    }


}
