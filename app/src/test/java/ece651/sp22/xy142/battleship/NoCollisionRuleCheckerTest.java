package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
    @Test
    public void test_no_collison_rule_only(){
        Board<Character> b = new BattleShipBoard<Character>(10, 20,'X');
        AbstractShipFactory<Character> fac = new V1ShipFactory();
        NoCollisionRuleChecker<Character> rule = new NoCollisionRuleChecker<>(null);
        Ship<Character> s =fac.makeSubmarine(new Placement("A0V"));
        assertEquals(null, rule.checkMyRule(s, b));
        b.tryAddShip(s);
        Ship<Character> c =fac.makeCarrier(new Placement("C0V"));
        assertEquals(null, rule.checkPlacement(c, b));
        b.tryAddShip(c);
        Ship<Character> d =fac.makeDestroyer(new Placement("D0V"));
        assertEquals("That placement is invalid: the ship overlaps another ship.", rule.checkMyRule(d, b));
    }

    @Test
    public void test_no_collison_rule_and_in_bound(){
        Board<Character> b = new BattleShipBoard<Character>(10, 20,'X');
        AbstractShipFactory<Character> fac = new V1ShipFactory();
        PlacementRuleChecker<Character> rule = new InBoundsRuleChecker<Character>(new NoCollisionRuleChecker<Character>(null));
        Ship<Character> s =fac.makeSubmarine(new Placement("A0V"));
        assertEquals(null, rule.checkPlacement(s, b));
        b.tryAddShip(s);
        Ship<Character> s2 =fac.makeSubmarine(new Placement("A0V"));
        assertEquals("That placement is invalid: the ship overlaps another ship.", rule.checkPlacement(s2, b));

         Ship<Character> c =fac.makeCarrier(new Placement("C0V"));
         assertEquals(null, rule.checkPlacement(c, b));
         b.tryAddShip(c);
         Ship<Character> d =fac.makeDestroyer(new Placement("S1V"));
         assertEquals("That placement is invalid: the ship goes off the bottom of the board.", rule.checkPlacement(d, b));
         Ship<Character> d2 =fac.makeDestroyer(new Placement("S8H"));
         assertEquals("That placement is invalid: the ship goes off the right of the board.", rule.checkPlacement(d2, b));

        }


    
}
