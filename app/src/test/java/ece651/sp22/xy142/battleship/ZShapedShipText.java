package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class ZShapedShipText {
    @Test
    public void test_constructor() {
        Ship<Character> s = new ZShapedShip<Character>(new Coordinate(0, 0), 'U', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(2, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(3, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(2, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(3, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(4, 1)));
        assertEquals(false, s.occupiesCoordinates(new Coordinate(0, 1)));
        assertThrows(IllegalArgumentException.class,()->new ZShapedShip<Character>(new Coordinate(0, 0), 'H', 'b', '*', "testShip"));
        assertEquals(new Coordinate(0,0), s.getRefCoordinate());


        assertEquals("testShip", s.getName());
        s = new ZShapedShip<Character>(new Coordinate(0, 0), 'D', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(2, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(2, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(3, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(4, 1)));
        assertEquals("testShip", s.getName());
        assertEquals(new Coordinate(4,1), s.getRefCoordinate());

        s = new ZShapedShip<Character>(new Coordinate(0, 0), 'L', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 2)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 3)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 2)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 3)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 4)));
        assertEquals("testShip", s.getName());
        assertEquals(new Coordinate(1,0), s.getRefCoordinate());

        s = new ZShapedShip<Character>(new Coordinate(0, 0), 'R', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1, 2)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 2)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 3)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0, 4)));
        assertEquals("testShip", s.getName());
        assertEquals(new Coordinate(0,4), s.getRefCoordinate());

    }
}