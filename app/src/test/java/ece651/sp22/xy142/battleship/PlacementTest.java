package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {

  @Test
  public void test_constructor() {
    Coordinate c1 = new Coordinate("A0");
    Placement p1 = new Placement(c1, 'H');
    Coordinate c2 = new Coordinate("A0");
     assertThrows(IllegalArgumentException.class,()-> new Placement(c2,'A'));
     Placement p2 = new Placement(c2,'H');
    assertEquals(c1, p1.getWhere());
    assertEquals('H', p1.getOrientation());
    assertEquals("(0,0)H", p1.toString());
    assertEquals(p1.hashCode(), p2.hashCode());
  }

  @Test
  public void test_equals() {
    Placement p1 = new Placement("A0H");
    Placement p2 = new Placement("A0H");
    Placement p3 = new Placement("A0V");
    Placement p4 = new Placement("A1H");
    Placement p5 = new Placement("B0V");
    assertEquals(p1, p2);
    assertEquals(false, p1.equals(p3));
    assertNotEquals(p1, p4);
    assertNotEquals(p2, p5);
    assertNotEquals(p1, "A0H");
  }

  @Test
  public void test_valid_constructor() {
    Placement p1 = new Placement("A0H");
    Placement p3 = new Placement("A0V");
    Placement p4 = new Placement("A9H");
    Placement p5 = new Placement("Z0V");
    assertEquals(0, p1.where.getRow());
    assertEquals(0, p1.where.getColumn());
    assertEquals('H', p1.orientation);
    assertEquals(0, p3.where.getRow());
    assertEquals(0, p3.where.getColumn());
    assertEquals('V', p3.orientation);
    assertEquals(0, p4.where.getRow());
    assertEquals(9, p4.where.getColumn());
    assertEquals('H', p4.orientation);

    assertEquals(25, p5.where.getRow());
    assertEquals(0, p5.where.getColumn());
    assertEquals('V', p5.orientation);
  }

  @Test
  public void test_invalid_constructor() {
    assertThrows(IllegalArgumentException.class, () -> new Placement("[0H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A11H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A-1H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("XaH"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("Z1P"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A"));
  }

}
