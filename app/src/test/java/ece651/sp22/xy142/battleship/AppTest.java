/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package ece651.sp22.xy142.battleship;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

class AppTest {
    @Test
    // not multi-threads safe so we need to add lock here
    //our test for main modifies the system streams. This means that we were to ever hava 
    //JUnit run our test in parallel with other test that uses System.ou, we could really get
    //mess up results.
    @ResourceLock(value =Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_main() throws IOException{
        ByteArrayOutputStream bytes =new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes,true);
        InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output.txt");
        assertNotNull(expectedStream);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try{
            System.setIn(input);
            System.setOut(out);
            App.main(new String[0]); // we donot need any arguments, so we just pass a 0 elment 
                                    // String[] (which is legal in java)
        }
        finally{
            System.setIn(oldIn);
            System.setOut(oldOut);
        }
        String expected = new String(expectedStream.readAllBytes());
        String actual = bytes.toString();
        assertEquals(expected, actual);



    }
}
