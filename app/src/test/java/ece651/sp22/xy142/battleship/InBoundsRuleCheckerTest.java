package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
    @Test
    public void test_in_bounds_rule(){
        Board<Character> b = new BattleShipBoard<>(10, 20,'X');
        AbstractShipFactory<Character> fac = new V1ShipFactory();
        InBoundsRuleChecker<Character> rule = new InBoundsRuleChecker<>(null);
        Ship<Character> s =fac.makeSubmarine(new Placement("A0V"));
        assertEquals(null, rule.checkMyRule(s, b));
        Ship<Character> c =fac.makeCarrier(new Placement("A8H"));
        assertEquals("That placement is invalid: the ship goes off the right of the board.", rule.checkPlacement(c, b));
        Ship<Character> d =fac.makeDestroyer(new Placement("S1V"));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.", rule.checkMyRule(d, b));
         Ship<Character> d2 =fac.makeDestroyer(new Placement(new Coordinate(-1,2),'V'));
         assertEquals("That placement is invalid: the ship goes off the top of the board.", rule.checkMyRule(d2, b));
         Ship<Character> d3 =fac.makeDestroyer(new Placement(new Coordinate(4,-5),'H'));
         assertEquals("That placement is invalid: the ship goes off the left of the board.", rule.checkMyRule(d3, b));

    }
}
