package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TShapedShipTest {
    @Test
    public void test_constructor(){
        Ship<Character> s = new TShapedShip<Character>(new Coordinate(0,0), 'U', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0,1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,2)));
        assertEquals(false, s.occupiesCoordinates(new Coordinate(1,3)));
        assertEquals("testShip", s.getName());
        assertEquals(new Coordinate(1,0), s.getRefCoordinate());
        s = new TShapedShip<Character>(new Coordinate(0,0), 'D', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0,0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0,1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0,2)));
        assertEquals(false, s.occupiesCoordinates(new Coordinate(1,3)));
        assertEquals("testShip", s.getName());
        assertEquals(new Coordinate(0,2), s.getRefCoordinate());

        s = new TShapedShip<Character>(new Coordinate(0,0), 'L', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0,1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(2,1)));
        assertEquals(false, s.occupiesCoordinates(new Coordinate(2,0)));
        assertEquals(new Coordinate(2,1), s.getRefCoordinate());

        assertEquals("testShip", s.getName());
        s = new TShapedShip<Character>(new Coordinate(0,0), 'R', 'b', '*', "testShip");
        assertEquals(true, s.occupiesCoordinates(new Coordinate(0,0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,0)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(1,1)));
        assertEquals(true, s.occupiesCoordinates(new Coordinate(2,0)));
        assertEquals(false, s.occupiesCoordinates(new Coordinate(2,1)));
        assertEquals(new Coordinate(0,0), s.getRefCoordinate());

        assertEquals("testShip", s.getName());


    }
    
}
