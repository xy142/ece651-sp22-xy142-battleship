package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_display_char() {
    ShipDisplayInfo<Character> shipDisp= new SimpleShipDisplayInfo<Character>('s', '*');
    assertEquals('s', shipDisp.getInfo(null, false));
    assertEquals('*', shipDisp.getInfo(null, true));    
  }

}
