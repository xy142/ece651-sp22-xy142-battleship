package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
  private void checkShip(Ship<Character> testShip, String expectedName, Character expectedLetter,
      Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for(int i =0; i<expectedLocs.length;i++){
          assertEquals(expectedLetter, testShip.getDisplayInfoAt(expectedLocs[i],true));
        }
        testShip.recordHitAt(expectedLocs[0]);
        assertEquals('*', testShip.getDisplayInfoAt(expectedLocs[0],true));
  }

  @Test
  public void test_make_ships() {
    Placement h1_2 = new Placement(new Coordinate(1,2), 'H');
    AbstractShipFactory<Character> fac = new V1ShipFactory();
    Ship<Character> s1 = fac.makeSubmarine(h1_2);
    checkShip(s1, "Submarine", 's',new Coordinate(1,2),new Coordinate(1,3));
    Placement v1_2 = new Placement(new Coordinate(1,2), 'V');
    Ship<Character> d1 =fac.makeDestroyer(v1_2);    
    checkShip(d1, "Destroyer", 'd', new Coordinate(1,2),new Coordinate(2,2),new Coordinate(3,2));
    Ship<Character> b1 = fac.makeBattleship(h1_2);
    checkShip(b1, "Battleship", 'b', new Coordinate(1,2),new Coordinate(1,3),
    new Coordinate(1,4),new Coordinate(1,5))
    ;
    Ship<Character> c1 = fac.makeCarrier(h1_2);
    checkShip(c1, "Carrier", 'c', new Coordinate(1,2),new Coordinate(1,3),
    new Coordinate(1,4),new Coordinate(1,5), new Coordinate(1,6),
    new Coordinate(1,7));
 
  }

}
