package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  @Test
  public void test_display_empty_2by2() {
    Board<Character> b1 = new BattleShipBoard<Character>(2, 2,'X');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader = "  0|1\n";
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + "A | A\n" + "B | B\n" + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }
  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11,20,'X');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10, 27,'X');
    assertThrows(IllegalArgumentException.class,()->new BoardTextView(wideBoard)) ;
    assertThrows(IllegalArgumentException.class,()->new BoardTextView(tallBoard)) ;
  }
  @Test
  public void test_display_empty_3by2(){
    Board<Character> b23 = new BattleShipBoard<Character>(3, 2,'X');
    BoardTextView view = new BoardTextView(b23);
    String expected ="  0|1|2\n" + "A | | A\n" + "B | | B\n"+ "  0|1|2\n";
    assertEquals(expected, view.displayMyOwnBoard());
  }
  @Test
  public void test_display_empty_2by3(){
    Board<Character> b32 = new BattleShipBoard<Character>(2, 3,'X');
    BoardTextView view = new BoardTextView(b32);
    String expected ="  0|1\n" + "A | A\n" + "B | B\n"+"C | C\n"+ "  0|1\n";
    assertEquals(expected, view.displayMyOwnBoard());
  }
  /**
   *note we can wrtie a private helper function here to help us check
   */

    @Test
  public void test_display_ships_2by3(){
    Board<Character> b32 = new BattleShipBoard<Character>(2, 3,'X');
    BoardTextView view = new BoardTextView(b32);
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(2,1),'s','*');
    b32.tryAddShip(s1);
    b32.tryAddShip(s2);
    String expected ="  0|1\n" + "As| A\n" + "B | B\n"+"C |sC\n"+ "  0|1\n";
    assertEquals(expected, view.displayMyOwnBoard());
  }

  @Test
  public void test_display_enemy_2by3(){
    Board<Character> b32 = new BattleShipBoard<Character>(2, 3,'X');
    BoardTextView view = new BoardTextView(b32);
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(2,1),'s','*');
    b32.tryAddShip(s1);
    b32.tryAddShip(s2);
    b32.fireAt(new Coordinate(0,1));
    b32.fireAt(new Coordinate(2,1));
    String expected ="  0|1\n" + "A |XA\n" + "B | B\n"+"C |sC\n"+ "  0|1\n";
    assertEquals(expected, view.displayEnemyBoard());
  }

  @Test
  public void test_display_two_boards_2by3(){
    //b32 is the enemyBoard
    Board<Character> b32 = new BattleShipBoard<Character>(2, 3,'X');
    BoardTextView view_b32 = new BoardTextView(b32);
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(2,1),'s','*');
    b32.tryAddShip(s1);
    b32.tryAddShip(s2);
    b32.fireAt(new Coordinate(0,1));
    b32.fireAt(new Coordinate(2,1));
    String expected_32 ="  0|1\n" + "A |XA\n" + "B | B\n"+"C |sC\n"+ "  0|1\n";
    assertEquals(expected_32, view_b32.displayEnemyBoard());
    //b is my board
    Board<Character> b = new BattleShipBoard<Character>(2, 3,'X');
    BoardTextView view = new BoardTextView(b);
    b.tryAddShip(s1);
    b.tryAddShip(s2);
    String expected ="  0|1\n" + "As| A\n" + "B | B\n"+"C |*C\n"+ "  0|1\n";
    assertEquals(expected, view.displayMyOwnBoard());
    String expectedTwo =
"     Your ocean      Player B's ocean\n"+
"  0|1      0|1\n"+
"As| A    A |XA\n"+
"B | B    B | B\n"+
"C |*C    C |sC\n"+
"  0|1      0|1\n";
    assertEquals(expectedTwo,view.displayMyBoardWithEnemyNextToIt(view_b32, "Your ocean",
     "Player B's ocean"));

  }



}
