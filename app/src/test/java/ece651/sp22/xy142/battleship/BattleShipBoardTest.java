package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10,20,'X');
    assertEquals(10,b1.getWidth());
    assertEquals(20,b1.getHeight());
    assertThrows(IllegalArgumentException.class, ()->new BattleShipBoard<Character>(10,0,'X'));
    assertThrows(IllegalArgumentException.class, ()->new BattleShipBoard<Character>(0,20,'X'));
    assertThrows(IllegalArgumentException.class, ()->new BattleShipBoard<Character>(10,-5,'X'));
    assertThrows(IllegalArgumentException.class, ()->new BattleShipBoard<Character>(-8,20,'X'));

  }
  private <T> void checkWhatIsAtBoard(Board<T> b, T[][] expected, boolean self, boolean innerTable){
    for (int i =0; i<expected.length;i++){
      for (int j =0; j <expected.length;j++){
        if (self){
        assertEquals(expected[i][j], b.whatIsAtForSelf(new Coordinate(i, j)));
        return;
        }
        assertEquals(expected[i][j], b.whatIsAtForEnemy(new Coordinate(i, j)));
      }
    }
  }

  @Test
  public void test_myShips() {
    Board<Character> b1 = new BattleShipBoard<Character>(2,2,'X');
    Character[][] expected= {{null, null}, {null,null}}; 
    checkWhatIsAtBoard(b1, expected,true,true);
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(1,0),'s','*');
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);
    Character[][] exp_now= {{'s', null}, {'s',null}};
    checkWhatIsAtBoard(b1, exp_now,true,true);
  }

  @Test
  public void test_fire_at(){
    Board<Character> b1 = new BattleShipBoard<Character>(2,2,'X');
    Character[][] expected= {{null, null}, {null,null}}; 
    checkWhatIsAtBoard(b1, expected,true,true);
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(1,0),'s','*');
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);   
    assertSame(s1, b1.fireAt(new Coordinate(0,0)));
    assertEquals(null, b1.fireAt(new Coordinate(0,1)));
    assertTrue(s1.isSunk());
    Character[][] exp_now= {{'*', null}, {'s',null}};
    checkWhatIsAtBoard(b1, exp_now,true,false);
    Ship<Character> s3 = new RectangleShip<Character>(new Coordinate(0,1), 1,2, 's','*',"test");
    b1.tryAddShip(s3);
    assertSame(s3, b1.fireAt(new Coordinate(1,1)));
    assertFalse(s3.isSunk());
    b1.fireAt(new Coordinate(0,1));
    assertTrue(s3.isSunk());
    b1.fireAt(new Coordinate(1,0));
    assertTrue(s2.isSunk());
    assertTrue(b1.ifAllShipsSunk());
  }

  @Test
  public void test_whatIsAtForEnemy(){
    Board<Character> b1 = new BattleShipBoard<Character>(2,2,'X');
    Character[][] expected= {{null, null}, {null,null}}; 
    checkWhatIsAtBoard(b1, expected,false,false);
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(1,0),'s','*');
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);   
    assertSame(s1, b1.fireAt(new Coordinate(0,0)));
    assertEquals(null, b1.fireAt(new Coordinate(0,1)));
    assertTrue(s1.isSunk());
    Character[][] exp_now= {{'s', 'X'}, {null,null}};
    checkWhatIsAtBoard(b1, exp_now,false,false);
    Ship<Character> s3 = new RectangleShip<Character>(new Coordinate(0,1), 1,2, 's','*',"test");
    b1.tryAddShip(s3);
    assertSame(s3, b1.fireAt(new Coordinate(1,1)));
    assertFalse(s3.isSunk());
  }

  @Test
  public void test_deleteShip_Move_Hits(){
    Board<Character> b1 = new BattleShipBoard<Character>(2,2,'X');
    Character[][] expected= {{null, null}, {null,null}}; 
    checkWhatIsAtBoard(b1, expected,true,false);
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(1,0),'s','*');
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);   
    b1.fireAt(new Coordinate(0,0));
    Character[][] exp1= {{'*', null}, {'s',null}};
    checkWhatIsAtBoard(b1, exp1,true,false);
    //delete s1
    b1.deleteShip(s1);
    Character[][] exp2= {{null, null}, {'s',null}};
    checkWhatIsAtBoard(b1, exp2,true,false);
    b1.moveHits(s1, s2);
    Character[][] exp3= {{null, null}, {'*',null}};
    checkWhatIsAtBoard(b1, exp3,true,false);
  }

  
}
