package ece651.sp22.xy142.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

public class TextPlayerTest {

  /**
   * Help us to create player much faster
   * 
   * @param w
   * @param h
   * @param inputData
   * @param bytes
   * @return
   */
  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
    AbstractShipFactory<Character> shipFactory = new V1ShipFactory();
    return new TextPlayer(board, input, output, "A", shipFactory);
  }

  private TextPlayer createTextPlayerV2(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
    AbstractShipFactory<Character> shipFactory = new V2ShipFactory();
    return new TextPlayer(board, input, output, "A", shipFactory);
  }

  @Test
  void test_read_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
    String prompt = "Please enter a location for a ship:";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');

    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(expected[i], p);
      assertEquals(prompt + "\n", bytes.toString()); // should have printed prompt and a new line
      bytes.reset(); // clear out bytes for next time around
    }
  } 

  @Test
  void test_read_coordinate() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2\nC8\na4\n", bytes);
    String prompt = "Please enter a location to hit:";
    Coordinate[] expected = new Coordinate[3];
    expected[0] = new Coordinate(1, 2);
    expected[1] = new Coordinate(2, 8);
    expected[2] = new Coordinate(0, 4);

    for (int i = 0; i < expected.length; i++) {
      Coordinate p = player.readCoordinate(prompt);
      assertEquals(expected[i], p);
      assertEquals(prompt + "\n", bytes.toString()); // should have printed prompt and a new line
      bytes.reset(); // clear out bytes for next time around
    }
  }



  @Test
  void test_do_one_placement() throws IOException {
    // StringReader sr = new StringReader("A1V\n");
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(3, 2, "A1V\n3333\na1v\n", bytes);
    String expected = "  0|1|2\n" + "A |s| A\n" + "B |s| B\n" + "  0|1|2\n";
    player.doOnePlacement("Submarine");
    assertEquals(expected, player.view.displayMyOwnBoard());
    bytes.reset();
  }
  @Test
  void test_do_one_placement_V2() throws IOException {
    // StringReader sr = new StringReader("A1V\n");
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayerV2(3, 2, "A0D", bytes);
    String expected = "  0|1|2\n" + "Ab|b|bA\n" + "B |b| B\n" + "  0|1|2\n";
    player.doOnePlacement("Battleship");
    assertEquals(expected, player.view.displayMyOwnBoard());
    bytes.reset();
  }

  // @Test
  // void test_do_placement_phase() throws IOException {
  //   ByteArrayOutputStream bytes = new ByteArrayOutputStream();
  //   TextPlayer player = createTextPlayer(3, 2, "A1V\na1v\nA1V\nA1V\nA1V\nA1V\nA1V\nA1V\nA1V\nA1V\nA1V\n"+
  //   "a1v\nA1V\nA1V\nA1V\nA1V\nA1V\nA1V\nA1V\nA1V\n", bytes);
  //   String expected = "  0|1|2\n" + "A |s| A\n" + "B |s| B\n" + "  0|1|2\n";
  //   player.doPlacementPhase();
  //   assertEquals(expected, player.view.displayMyOwnBoard());
  //   // one more test to check placementRule
  //   player.doPlacementPhase();
  //   // nothing should change
  //   assertEquals(expected, player.view.displayMyOwnBoard());
  // }
  /**
   * check eof handled 
   */
  @Test
  public void test_eof() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(3, 2, "", bytes);
    assertThrows(EOFException.class, ()->player.readPlacement("enter you location"));
    assertThrows(EOFException.class,()->player.doPlacementPhase());

  }

  @Test
  public void test_lose_game() throws IOException{
    Board<Character> b1 = new BattleShipBoard<Character>(2,2,'X');
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    BufferedReader input = new BufferedReader(new StringReader(" "));
    PrintStream output = new PrintStream(bytes, true);
    AbstractShipFactory<Character> shipFactory = new V1ShipFactory();
    TextPlayer p = new TextPlayer(b1, input, output, "A", shipFactory);
    b1.tryAddShip(s1);
    assertFalse(p.checkWin(p));
    b1.fireAt(new Coordinate(0,0));
    assertTrue(p.checkWin(p));
  }

   @Test
  public void test_play_one_turn() throws IOException{
    Board<Character> b1 = new BattleShipBoard<Character>(2,2,'X');
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    BufferedReader input = new BufferedReader(new StringReader("1\nA0\n"));
    PrintStream output = new PrintStream(bytes, true);
    AbstractShipFactory<Character> shipFactory = new V1ShipFactory();
    TextPlayer p = new TextPlayer(b1, input, output, "A", shipFactory);
    b1.tryAddShip(s1);
    p.playOneTurn(p);
    // String expected = 
    // "---------------------------------------------------------------------------\n"+
    // "Possible actions for Player A:\n"+
    // "1. Fire at a square\n"+
    // "2. Move a ship to another square (3 remaining)\n"+
    // "3. Sonar scan (3 remaining)\n"+
    // "Player A, what would you like to do?\n"+
    // "---------------------------------------------------------------------------\n"+
    
    // "Which coordinate do you want to fire at? Enter:e.g.'A0','B2'\n"+
    // "-------------------------------------------------------\n"+
    // "You hit a testship!\n"+
    // "-------------------------------------------------------\n";
    // assertEquals(expected, bytes.toString());
  }
  // @Test
  // public void test_doAttackingPhase() throws IOException{
  //   Board<Character> b1 = new BattleShipBoard<Character>(2,2,'X');
  //   Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0),'s','*');
  //   ByteArrayOutputStream bytes = new ByteArrayOutputStream();
  //   BufferedReader input = new BufferedReader(new StringReader("A0\n"));
  //   PrintStream output = new PrintStream(bytes, true);
  //   AbstractShipFactory<Character> shipFactory = new V1ShipFactory();
  //   TextPlayer p = new TextPlayer(b1, input, output, "A", shipFactory);
  //   b1.tryAddShip(s1);
  //   p.doAttackingPhase(p);
  // //   String expected=
  // "     Your ocean      Player A's ocean\n"+
  // "  0|1      0|1\n"+
  // "As| A    A | A\n"+
  // "B | B    B | B\n"+
  // "  0|1      0|1\n"+
  //   "Which coordinate do you want to fire at? Enter:e.g.'A0','B2'\n"+
  //   "-------------------------------------------------------\n"+
  //   "You hit a testship!\n"+
  //   "-------------------------------------------------------\n"+
  //   "Player A You are the winner!\n"+
  //   "Player A Oops..You lose this game..\n";
    //assertEquals(expected, bytes.toString());
  //}


}
