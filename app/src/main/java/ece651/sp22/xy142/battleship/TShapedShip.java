package ece651.sp22.xy142.battleship;

import java.util.HashSet;

public class TShapedShip<T> extends BasicShip<T> {
    final String name;
    protected Character orintation;

    /**
     * Statics methods:Put all the coordinates a ship into a set(since we do not
     * care about the order)
     * 
     * @param upperLeft
     * @param oritation here should be the right character
     * @throws IllegalArgumnet if oritation is not right
     * @return hashSet that contains all the coordinates
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, Character orintation) {
        if (orintation != 'U' && orintation != 'D' && orintation != 'L' && orintation != 'R') {
            throw new IllegalArgumentException("The orination is incorrect");
        }
        HashSet<Coordinate> ans = new HashSet<Coordinate>();
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        /**
         * here the design is not so good, violate the open/close princple
         * can do better by take an interface palceRuler
         * but shince time is limited, maybe do it in future
         */
        if (orintation == 'U') {
            ans.add(new Coordinate(row, col + 1));
            ans.add(new Coordinate(row + 1, col));
            ans.add(new Coordinate(row + 1, col + 1));
            ans.add(new Coordinate(row + 1, col + 2));
        } else if (orintation == 'R') {
            ans.add(new Coordinate(row, col));
            ans.add(new Coordinate(row + 1, col));
            ans.add(new Coordinate(row + 1, col + 1));
            ans.add(new Coordinate(row + 2, col));
        } else if (orintation == 'D') {
            ans.add(new Coordinate(row, col));
            ans.add(new Coordinate(row, col + 1));
            ans.add(new Coordinate(row + 1, col + 1));
            ans.add(new Coordinate(row, col + 2));
        } else if (orintation == 'L') {
            ans.add(new Coordinate(row, col + 1));
            ans.add(new Coordinate(row + 1, col + 1));
            ans.add(new Coordinate(row + 1, col));
            ans.add(new Coordinate(row + 2, col + 1));
        }
        return ans;
    }

    /**
     * A consttuctor that call parent BasicShip constructor
     * 
     * @param uppCoordinate
     * @param orintation
     * @param disp
     * @param name
     * @param enemyDisplayInfo
     */
    public TShapedShip(Coordinate uppCoordinate, Character orintation,
            ShipDisplayInfo<T> disp, String name, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(uppCoordinate, makeCoords(uppCoordinate, orintation), disp, enemyDisplayInfo);
        this.name = name;
        this.orintation = orintation;
    }

    /**
     * A quick convience constructors(initialize shipDisplay )that takes T data and
     * T onHit as params
     * use 'Simple'shipdisplayinfo class
     * 
     * This is the constructor we use mostly by default the enemyboard view
     * 
     * @param upperLeft
     * @param width
     * @param height
     * @param data
     * @param onHit
     */
    public TShapedShip(Coordinate upperLeft, Character orintation,
            T data, T onHit, String name) {
        this(upperLeft, orintation, new SimpleShipDisplayInfo<T>(data, onHit), name,
                new SimpleShipDisplayInfo<T>(null, data));
    }

    @Override
    public String getName() {
        return this.name;
    }

    /**
     * The reference coordinate for T ship is the upperMiddle coordinate
     * *b*    
     * Bbb
     * Capital B is the ref
     */
    @Override
    public Coordinate getRefCoordinate() {
        int row = this.upperLeft.getRow();
        int col = this.upperLeft.getColumn();
        if (this.orintation == 'U') {
            return new Coordinate(row+1, col);
        } else if (this.orintation == 'D') {
            return new Coordinate(row , col + 2);
        } else if (this.orintation == 'R') {
            return new Coordinate(row, col );
        } else {
            return new Coordinate(row +2, col+1);
        }
    }

}
