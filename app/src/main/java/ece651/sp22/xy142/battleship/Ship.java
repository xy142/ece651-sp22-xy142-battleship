package ece651.sp22.xy142.battleship;

import java.util.Collection;

/**This interface repesents any type of ship in our battleship game. It is
   *generic in typename T, which is the type of information the view needs to display this ship
   */
public interface Ship<T> {
  /** 
   * check if this ship occupies the given coordinate
   *
   *@param where is the Coordinate to check if the Ship occupies
   *@return true if where is inside this ship, false if not
   */
  public boolean occupiesCoordinates(Coordinate where);
  /**
   *Check if the ship has been hit in all of its locations menaing it has been sunk.
   *
   *@return true if this ship has been sunk,fasle otherwise.
   */
  public boolean isSunk();
  /**
   *Make this ship record that it has been hit at the given coordinate. 
   * The specified coordinate must be part of the ship
   *
   *@param where specifies the coordinates that were hit
   *@throws IllegalArgumentException if where is not part of the ship
   */
  public void recordHitAt(Coordinate where);
  /**
   *check if the hisp was hit at the a specific coordinates. The coordinates must be part of the ship
   *@param where is the coordinate to check
   *@return false if this ship as not hit at the coordinate, otherwise true
   *@throws IllegalArgumentException if the coordinate are not part of the ship
   */
  public boolean wasHitAt(Coordinate where, boolean self);
  /**
   *return the view-specific info at the given coordinate. 
   *This coordinate must be part of the ship
   *@param coordinate
   *@param myShip boolean to see if this on my board
   *@throws IllegalArgumentException if coordinates do not belong to ship
   *return the view-specfic info at that coordinate
   */

  public T getDisplayInfoAt(Coordinate where, boolean myShip);

  /**
   * Get the name of this Ship, such as "submarine"
   * @return the name of this ship
   */
  public String getName();
  

  public T getReprentLetter();
  /**
   * Get all of the Coordinates that this Ship occupies
   * @return an Iterable with the coordinates that this Ship occupies
   */
  public Iterable<Coordinate> getCoordinates();
  /**
   * get the reference coordinate in a ship, it will changed when ship move or have 
   * diferent orintation
   * this is useful when dealing with moving
   * via this reference, we can find the relative hit
   * @return  refernce point
   */
  public Coordinate getRefCoordinate();
  /**
   * set the relativeHits field for new ship constructed by move action
   */
  public void setRelativeHits(Collection<Coordinate> oldRelativeHits);
  public Collection<Coordinate> getRelativeHits();

}
