package ece651.sp22.xy142.battleship;

public interface ShipDisplayInfo<T> {
    /**
     * return what to display depends on wheter been hitted 
     * @param where
     * @param hit
     * @return pattern
     */
    public T getInfo(Coordinate where, boolean hit);
}
