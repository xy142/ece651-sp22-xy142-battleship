package ece651.sp22.xy142.battleship;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Construct a BattleShipBoar with the specific width and height
 * 
 * @param width  is the width of the newly constructed board
 * @param height is the height of the newly constructed board
 * @throw IllegalArgumentException if the width or height are less than or equal
 *        to zero
 */

public class BattleShipBoard<T> implements Board<T> {
  private final int width;
  private final int height;
  final ArrayList<Ship<T>> myShips;
  private final PlacementRuleChecker<T> placementRuleChecker;
  // saved information like miss or hit for display on enemyboard
  private final HashMap<Coordinate,T> enemySavedInfo;
  final T missInfo;

  public BattleShipBoard(int width, int height, PlacementRuleChecker<T> rule, T missInfo) {
    if (width <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is" + width);
    }
    if (height <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is" + height);
    }
    this.width = width;
    this.height = height;
    this.myShips = new ArrayList<Ship<T>>();
    this.placementRuleChecker = rule;
    this.enemySavedInfo = new HashMap<Coordinate,T>();
    this.missInfo = missInfo;
  }

  /**
   * the actual default constructor we use, by default two rules in-boundas and
   * no-overlapping
   * are employed.
   * 
   * @param width
   * @param height
   * @param missInfo
   */
  public BattleShipBoard(int width, int height, T missInfo) {
    this(width, height, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null)), missInfo);
  }

  public String tryAddShip(Ship<T> toAdd) {
    String exceptionMeg = placementRuleChecker.checkPlacement(toAdd, this);
    if (exceptionMeg == null) {
      myShips.add(toAdd);
    }
    return exceptionMeg;
  }

  /**
   * NOTE:NEED TO CHANGE THIS DESCRIPTION LATER
   * retrun null if no ships on it
   * else call getDisplatInfoAt to return corresponing pattern
   * aslo depends on views whether on my board or other board
   */
  protected T whatIsAt(Coordinate where, boolean isSelf) {
    // first check if this area has already saved in board as miss before 
    if (!isSelf && enemySavedInfo.containsKey(where)) {
      return enemySavedInfo.get(where);
    }
    for (Ship<T> s : myShips) {
      if (s.occupiesCoordinates(where)) {
        return s.getDisplayInfoAt(where, isSelf);
      }
    }
    return null;
  }

  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  public T whatIsAtForEnemy(Coordinate where) {
    return whatIsAt(where, false);
  }

  /**
   * 
   * This method should search for any ship that occupies coordinate c
   * (you already have a method to check that)
   * If one is found, that Ship is "hit" by the attack and should
   * record it (you already have a method for that!). Then we
   * should return this ship.
   * !!plus for V2, we need to trcak this hit to our borad for later display to enemy
   * 
   * If no ships are at this coordinate, we should record
   * the miss in the enemyMisses HashSet that we just made,
   * and return null.
   * 
   * the board call this function should be the competer's board
   * 
   * @param c
   * @return ship if hitted; otherwise null
   */
  public Ship<T> fireAt(Coordinate c) {
    for (Ship<T> s : myShips) {
      // this means the ship is hitted, we should record an return
      if (s.occupiesCoordinates(c)) {
        s.recordHitAt(c);
        enemySavedInfo.put(c, s.getReprentLetter());
        return s;
      }
    }
    // no ship find
    // enemyMisses.add(c);
    enemySavedInfo.put(c,missInfo);
    return null;
  }

  /**
   * this function will return true if all the ships in the board are sunk
   */
  public boolean ifAllShipsSunk() {
    for (Ship<T> s : myShips) {
      if (!s.isSunk()) {
        return false;
      }
    }
    return true;
  }

  /**
   * 1.get the relative hits coordinate of the old ship
   * 2. assgin the relativeHits to the new ship's relativeHits
   * 2. get the reference point of the new ship
   */
  public void moveHits(Ship<T> oldShip, Ship<T> newShip) {
    newShip.setRelativeHits(oldShip.getRelativeHits());
  }

  public void deleteShip(Ship<T> toDel) {
    for (Ship<T> s : this.myShips) {
      if (s == toDel) {
        this.myShips.remove(s);
        return;
      }
    }
  }

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  @Override
  public Ship<T> selectShipByPart(Coordinate c) {
    for (Ship<T> s : myShips) {
      // this means the ship is hitted, we should record an return
      if (s.occupiesCoordinates(c)) {
        s.recordHitAt(c);
        return s;
      }
    }
    return null;
  }
}
