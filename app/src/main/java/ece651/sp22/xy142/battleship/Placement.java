package ece651.sp22.xy142.battleship;


public class Placement {
  final Coordinate where;
  final char orientation;

  public Placement(Coordinate where, char orientation) {
    this.where = new Coordinate(where.getRow(), where.getColumn());
    if (!(orientation == 'V' || orientation == 'H'||orientation == 'U'||
    orientation =='D' || orientation == 'L'||orientation == 'R')) {
      throw new IllegalArgumentException("The orientation for your placement is wrong");
    }
    this.orientation = orientation;
  }

  public Placement(String descr){
    if (descr.length() != 3) {
      throw new IllegalArgumentException("The length for your placement is wrong");
    }
    descr = descr.toUpperCase();
    this.where = new Coordinate(descr.substring(0, 2));
    this.orientation = descr.charAt(2);
    if (!(orientation == 'V' || orientation == 'H'||orientation == 'U'||
    orientation =='D' || orientation == 'L'||orientation == 'R')) {
      throw new IllegalArgumentException("The orientation for your placement is wrong");
    }

  }

  public Coordinate getWhere() {
    return this.where;
  }

  public char getOrientation() {
    return orientation;
  }

  @Override
  public boolean equals(Object o) {
    if (o != null && o.getClass().equals(getClass())) {
      Placement p = (Placement) o;
      return where.equals(p.where) && orientation == p.orientation;
    }
    return false;
  }

  @Override
  public String toString() {
    return this.where.toString() + this.orientation;
  }

  @Override
  public int hashCode() {
    return this.toString().hashCode();
  }

}
