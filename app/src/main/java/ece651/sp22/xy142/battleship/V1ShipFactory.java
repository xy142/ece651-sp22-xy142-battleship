package ece651.sp22.xy142.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {
    /**
     * This a helper function to help make ships, avoid repeated codes
     * 
     * @param where  Placement type need to converted to Coordinte
     *               Note: we assumne all the ships are in a vertical line, i.e w =1
     *               Hence, if oritantion is H, do nothing but V need to reverse.
     * @param w      width
     * @param h      height
     * @param letter reprentation to display, e.g. 's'
     * @param name   name of the ship
     * @return specific type of ship
     */

    protected Ship<Character> createShip(Placement where, int w,
            int h, char letter, String name) {
        Coordinate pos = where.getWhere();
        int width = w;
        int height = h;
        if (where.getOrientation() == 'H') {
            width = h;
            height = w;
        }
        return new RectangleShip<Character>(pos, width, height, letter, '*', name);
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    };

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

}
