package ece651.sp22.xy142.battleship;

public class Coordinate {
  private final int row;
  private final int column;
  
  public Coordinate(int row, int col) {
    this.row = row;
    this.column = col;
  }

  public Coordinate(String descr) {
    if (descr.length()<2){
      throw new IllegalArgumentException("The format of coordinate is wrong");
    }
    String Upcase = descr.toUpperCase();
    row = Upcase.charAt(0) - 'A';
    if (row < 0 || row > 25) {
      throw new IllegalArgumentException("The row should between 'A-Z'");
    }
    String numS = Upcase.substring(1);
    try{
    column = Integer.valueOf(numS);
    }
    catch(NumberFormatException e){
      throw new IllegalArgumentException("The column should be a number");
    }
    if (column <0 || column>9){
      throw new IllegalArgumentException("The column should between '0-9'");
    }
  }
  // /**
  //  *This is a copy constructor for Coordinate
  //  */
  // public Coordinate( Coordinate another){
  //   this.row = another.row;
  //   this.column = another.column;
  // }
  public int getRow() {
    return this.row;
  }

  public int getColumn() {
    return column;
  }

  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Coordinate c = (Coordinate) o;
      return row == c.row && column == c.column;
    }
    return false;
  }

  @Override
  public String toString() {
    return "(" + row + "," + column + ")";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

}
