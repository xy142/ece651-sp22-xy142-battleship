package ece651.sp22.xy142.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
    private final String name;

    /**
     * Statics methods:Put all the coordinates a ship into a set(since we do not
     * care about the order)
     * 
     * @param upperLeft
     * @param width
     * @param height
     * @return hashSet that contains all the coordinates
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> ans = new HashSet<Coordinate>();
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                ans.add(new Coordinate(row + j, col + i));
            }
        }
        return ans;
    }

    public RectangleShip(Coordinate uppCoordinate, int width, int height,
            ShipDisplayInfo<T> disp, String name,ShipDisplayInfo<T> enemyDisplayInfo) {
        super(uppCoordinate,makeCoords(uppCoordinate, width, height), disp,enemyDisplayInfo);
        this.name = name;    
    }

    /**
     * A quick convience constructors that takes T data and T onHit as params
     * use 'Simple'shipdisplayinfo class
     * 
     * This is the constructor we use mostly ofr version 1. by default the enemyboard view
     * @param upperLeft
     * @param width
     * @param height
     * @param data
     * @param onHit
     */
    public RectangleShip(Coordinate upperLeft, int width, int height,
            T data, T onHit,String name) {
        this(upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),name,
        new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * Stil a quick constructor for a 1x1 size ship using chain constructing
     * 
     * @param upperLeft
     * @param data
     * @param onHit
     */
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this(upperLeft, 1, 1, data, onHit,"testship");
    }

    @Override
    public String getName() {
        return this.name;
    }

    

    /**
     * by default this point is its upperleft point
     */
    @Override
    public Coordinate getRefCoordinate() {
        return this.upperLeft;
    }

}
