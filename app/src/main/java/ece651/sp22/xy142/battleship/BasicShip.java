package ece651.sp22.xy142.battleship;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


public abstract class BasicShip<T> implements Ship<T> {
  /**
   * if myPieces.get(c) is null -> c is not a part of ship
   * if myPieces.get(c) is true -> a hitted part of ship
   * if myPieces.get(c) is fals -> a non-hitted part of ship
   */
  // NOTE: Here the myPieces has the same function as relativeHits
  //Currently I do not have time to change this, may be improve it in the future
  protected HashMap<Coordinate,Boolean> myPieces;
  protected ShipDisplayInfo<T> myDisplayInfo;
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  protected Coordinate upperLeft;
  //this arraylist is to save relative coordiante(against the reference coordinate) of hitted parts 
  protected ArrayList<Coordinate> relativeHits; 
                                                

  public BasicShip(Coordinate upperLeft, Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, 
  ShipDisplayInfo<T> enemyDisplayInfo){
    myPieces = new HashMap<Coordinate,Boolean>();
    for (Coordinate c: where){
      myPieces.put(c,false);
    }
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    this.upperLeft = upperLeft;
    this.relativeHits = new ArrayList<Coordinate>();
  }

  /**
   * Check if a cell been occupied
   */
  @Override
  public boolean occupiesCoordinates(Coordinate where) {
    for(Coordinate c : myPieces.keySet()){
      if (where.equals(c)){
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isSunk() {
    if(relativeHits.size() ==myPieces.size()){
      return true;
    }
    else{
      return false;
    }
    // for (boolean b :myPieces.values()){
    //   if (!b){
    //     return false;
    //   }
    // }
    // return true;
  }
  /**
   * since these three methods need to throw an exception if coordinate is
   * not in the ship
   * we abstract this function out 
   * @throws IllegalArgumentException
   */
  protected void checkCoordinateInThisShip(Coordinate c){
    if(!occupiesCoordinates(c)){
      throw new IllegalArgumentException("This coordinate is not in the ship ");
    }
  }


  @Override
  public void recordHitAt(Coordinate where) {
    checkCoordinateInThisShip(where);
    myPieces.put(where, true);
    //compute the abslute difference with reference point
    Coordinate ref = this.getRefCoordinate();
    int row_abs_relative = Math.abs(where.getRow()-ref.getRow());
    int col_abs_relative = Math.abs(where.getColumn()-ref.getColumn());
    Coordinate newCoor = new Coordinate(row_abs_relative,col_abs_relative);
    for(Coordinate c : this.relativeHits){
      if(c.equals(newCoor)){
        return; // do not add repeated part
      }
    }
    relativeHits.add(newCoor);
  }

  @Override
  public boolean wasHitAt(Coordinate where,boolean innerTable) {
    checkCoordinateInThisShip(where);
    if(innerTable == false){ //for the aim to display for the enemy, only check if the table mark true
    return myPieces.get(where);
    }
    // for the aim to self, need to go to the relative hits to see if hit
    Coordinate ref = this.getRefCoordinate();
    int row_abs_relative = Math.abs(where.getRow()-ref.getRow());
    int col_abs_relative = Math.abs(where.getColumn()-ref.getColumn());
    Coordinate newCoor = new Coordinate(row_abs_relative,col_abs_relative);
    for(int i =0; i< relativeHits.size();i++){
      if (relativeHits.get(i).equals(newCoor)){
        return true;
      }
    }
    return false;
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean myShip){
    checkCoordinateInThisShip(where);
    if (myShip){
      return myDisplayInfo.getInfo(where, wasHitAt(where,true));
    }
    return enemyDisplayInfo.getInfo(where, wasHitAt(where,false));
  }
  @Override
  public Iterable<Coordinate> getCoordinates(){
    return myPieces.keySet();
  }

  @Override
  public void setRelativeHits(Collection<Coordinate> old){
    this.relativeHits = new ArrayList<Coordinate>(old);
  }

  @Override
  public Collection<Coordinate> getRelativeHits(){
    return this.relativeHits;
  }
 @Override
 public T getReprentLetter(){
   return enemyDisplayInfo.getInfo(null, true);
 }



  // @Override
  // public void updateHitsAfterMove(){
  //   Coordinate ref = this.getRefCoordinate();
  //   for(int i =0; i< relativeHits.size();i++){
  //     for(Map.Entry<Coordinate,Boolean> it: this.myPieces.entrySet()){
  //       int row_rel = Math.abs(it.getKey().getRow()-ref.getRow());
  //       int col_rel = Math.abs(it.getKey().getColumn()-ref.getColumn());
  //       if(relativeHits.get(i).equals( new Coordinate(row_rel,col_rel))){
  //         myPieces.put(it.getKey(), true);
  //         continue;
  //       }
  //     }
  //   }
  // }

}
