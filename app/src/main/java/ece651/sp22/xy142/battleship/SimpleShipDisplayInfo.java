package ece651.sp22.xy142.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    private final T mydata;
    private final T onHit;
    public SimpleShipDisplayInfo(T mydata, T onHit){
        this.mydata = mydata;
        this.onHit = onHit;
    }

    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit){
            return onHit;
        }
        return mydata;
    }
    
}
