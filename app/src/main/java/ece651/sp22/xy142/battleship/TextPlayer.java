package ece651.sp22.xy142.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
    /**
     * Not sure whether the variable here need to be declared as private
     */
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    private int moveActionNum;
    private int sonarActionNum;

    /**
     * constructor for player
     * 
     * @param theBoard
     * @param inputSource
     * @param out
     * @param name
     * @param factory
     * @param moveActionNum  customize how many actions you need
     * @param sonarActionNum
     */
    public TextPlayer(Board<Character> theBoard, BufferedReader inputSource, PrintStream out, String name,
            AbstractShipFactory<Character> factory, int moveActionNum, int sonarActionNum) {
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputSource;
        this.out = out;
        this.shipFactory = factory;
        this.name = name;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        setupShipCreationList();
        setupShipCreationMap();
        this.moveActionNum = moveActionNum;
        this.sonarActionNum = sonarActionNum;

    }

    /**
     * This is the default construcotr in app, make 3 times for addtional actions
     * 
     * @param theBoard
     * @param inputSource
     * @param out
     * @param name
     * @param factory
     */
    public TextPlayer(Board<Character> theBoard, BufferedReader inputSource, PrintStream out, String name,
            AbstractShipFactory<Character> factory) {
        this(theBoard, inputSource, out, name, factory, 3, 3);
    }

    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("You input nothing");
        }
        try {
            return new Placement(s);
        } catch (IllegalArgumentException e) {
            out.println("That placement is invalid: it does not have the correct format.");
            return readPlacement(prompt);
        }
    }

    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("You input nothing");
        }
        try {
            return new Coordinate(s);
        } catch (IllegalArgumentException e) {
            out.println("That coordinate is invalid: it does not have the correct format.");
            return readCoordinate(prompt);
        }
    }

    /**
     * For v2, we add a check here to make sure orintaion is correct for ship type
     * 
     * @throws IOException
     */
    public void doOnePlacement(String shipName) throws IOException {
        Placement pos = null;
        try {
            pos = readPlacement("Player " + this.name +
                    " Where do you want to place a " + shipName + "?");
        } catch (IOException ioe) {
            out.println(ioe.getLocalizedMessage());
            System.exit(1);
        }
        if (shipName.equals("Battleship") || shipName.equals("Carrier")) {
            if (pos.getOrientation() == 'V' || pos.getOrientation() == 'H') {
                this.out.println("This type of ship doesn't have that orintation");
                doOnePlacement(shipName);
                return;
            }
        }
        if (shipName.equals("Submarine") || shipName.equals("Destroyer")) {
            if (pos.getOrientation() == 'U' || pos.getOrientation() == 'D' ||
                    pos.getOrientation() == 'L' || pos.getOrientation() == 'R') {
                this.out.println("This type of ship doesn't have that orintation");
                doOnePlacement(shipName);
                return;
            }
        }
        Ship<Character> s = shipCreationFns.get(shipName).apply(pos);
        String mesg = theBoard.tryAddShip(s);
        if (mesg != null) {
            out.println(mesg);
            doOnePlacement(shipName);
            return;
        }
        out.println(view.displayMyOwnBoard());
    }

    /**
     * (a) display the starting (empty) board
     * (b) print the instructions message (from the README,
     * but also shown again near the top of this file)
     * (c) call doOnePlacement to place one ship
     * 
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        out.println(this.view.displayMyOwnBoard());
        String message = "--------------------------------------------------------------------------\n"
                + "Player " + this.name
                + ": you are going to place the following ships (which are all\n"
                + "rectangular). For each ship, type the coordinate of the upper left\n"
                + "side of the ship, followed by either H (for horizontal) or V (for \n"
                + "vertical).  For example M4H would place a ship horizontally starting\n"
                + "at M4 and going to the right.  You have\n"
                + '\n'
                + "2 'Submarines' ships that are 1x2\n"
                + "3 'Destroyers' that are 1x3\n"
                + "3 'Battleships' that are 1x4\n"
                + "2 'Carriers' that are 1x6\n"
                + "----------------------------------------------------------------------------";
        out.println(message);
        for (String shipname : this.shipsToPlace) {
            this.doOnePlacement(shipname);
        }
    }

    public int makeChoices() throws IOException {
        String prompt = "---------------------------------------------------------------------------\n" +
                "Possible actions for Player " + this.name + ":\n" + '\n' +
                "1. Fire at a square\n" +
                "2. Move a ship to another square (" + Integer.toString(moveActionNum) + " remaining)\n" +
                "3. Sonar scan (" + Integer.toString(sonarActionNum) + " remaining)\n" + '\n' +
                "Player " + this.name + ", what would you like to do?\n" +
                "---------------------------------------------------------------------------\n";
        this.out.println(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException("You input nothing");
        }
        int num = 0;
        try {
            num = Integer.valueOf(s);
        } catch (NumberFormatException e) {
            out.println("Oops!");
            out.println("Your input is not integer, try agin");
            return makeChoices();
        }
        if (num != 1 && num != 2 && num != 3) {
            out.println("Oops!");
            out.println("Your input is not 1,2,3, try agin");
            return makeChoices();
        }
        return num;
    }

    /**
     * call this method afrer palyer A finish attacking
     * check if the user loses the game
     * 
     * @return true if a loss
     */
    public boolean checkWin(TextPlayer enemy) {
        if (enemy.theBoard.ifAllShipsSunk()) {
            this.out.println("Player " + this.name + " You are the winner!");
            enemy.out.println("Player " + enemy.name + " Oops..You lose this game..");
            return true;
        }
        return false;
    }

    /**
     * 1. display the two boards for the player
     * 2. prompt for the coordinate to fire at
     * 3. prompt whether hit
     * 
     * @param enemy
     */
    public void playOneTurn(TextPlayer enemy) throws IOException {
        int choice = makeChoices();

        if (choice == 1) {
            Coordinate c = null;
            try {
                c = readCoordinate("Which coordinate do you want to fire at? Enter:e.g.'A0','B2'");
            } catch (IOException ioe) {
                out.println(ioe.getLocalizedMessage());
                System.exit(1);
            }
            Ship<Character> hit = enemy.theBoard.fireAt(c);
            if (hit == null) {
                this.out.println("-------------------------------------------------------");
                this.out.println("You missed!");
                this.out.println("-------------------------------------------------------");
            } else {
                this.out.println("-------------------------------------------------------");
                this.out.println("You hit a " + hit.getName() + "!");
                this.out.println("-------------------------------------------------------");
            }
        }

        else if (choice == 2) {
            if (!this.moveAction()) {
                playOneTurn(enemy);
            }
        } else {
            if(!this.scanAction(enemy)){
                playOneTurn(enemy);
            }
        }
    }

    public boolean doAttackingPhase(TextPlayer enemy) throws IOException {
        this.out.println(view.displayMyBoardWithEnemyNextToIt(enemy));
        this.playOneTurn(enemy);
        return this.checkWin(enemy);
    }

    public boolean moveAction() throws IOException {
        String prompt = "Please select which ship you want to move by entring coordinate of one part of this ship.";
        Coordinate c = null;
        try {
             c= readCoordinate(prompt);
        } catch (IOException ioe) {
            out.println(ioe.getLocalizedMessage());
            System.exit(1);
        }
        Ship<Character> shipSel = this.theBoard.selectShipByPart(c);
        if (shipSel == null) {
            out.println("There is no ship at that location");
            out.println("Please re-enter a valid location");
            return moveAction();
        }
        return moveHelper(shipSel);
    }

    /**
     * 1. delete it from the board(by deleting it from arraylist myShips)
     * 2. try to build a new ship at that location
     * 3. call the move() method in the board to delete the old one and add relative
     * hits
     * on the new ship
     * 
     * @param oldShip
     * @param newPlacement
     */
    public boolean moveHelper(Ship<Character> oldShip) throws IOException {
        String shipName = oldShip.getName();
        this.theBoard.deleteShip(oldShip);
        Placement pos = readPlacement("Player " + this.name +
                " Where do you want to move your " + shipName + " to?");

        if (shipName.equals("Battleship") || shipName.equals("Carrier")) {
            if (pos.getOrientation() == 'V' || pos.getOrientation() == 'H') {
                this.out.println("This type of ship doesn't have that orintation");
                return moveHelper(oldShip);
            }
        }
        if (shipName.equals("Submarine") || shipName.equals("Destroyer")) {
            if (pos.getOrientation() == 'U' || pos.getOrientation() == 'D' ||
                    pos.getOrientation() == 'L' || pos.getOrientation() == 'R') {
                this.out.println("This type of ship doesn't have that orintation");
                return moveHelper(oldShip);
            }
        }
        Ship<Character> newShip = shipCreationFns.get(shipName).apply(pos);
        String mesg = theBoard.tryAddShip(newShip);
        if (mesg != null) {
            out.println("---------------------------------------------------------------");
            out.println(mesg);
            out.println("Moving ship fails");
            out.println("---------------------------------------------------------------");
            this.theBoard.tryAddShip(oldShip);
            return false;
        } else { // we have add the newShip on our board
            this.theBoard.moveHits(oldShip, newShip);
            out.println("---------------------------------------------------------------");
            out.println("You seccessfully move the ship! This is your new board!");
            out.println(view.displayMyOwnBoard());
            out.println("---------------------------------------------------------------");
            return true;
        }
    }
    //rowNum =4, cloNum =4
    ArrayList<Coordinate> scanPieces(Coordinate center, int rowNum, int colNum) {
        ArrayList<Coordinate> ans = new ArrayList<>();
        for (int row = 0; row < rowNum; row++) {
            for (int col = 0; col < colNum; col++) {
                try{
                ans.add(new Coordinate(center.getRow() + row, center.getColumn() + col));
                }
                catch(IllegalArgumentException e){}//do nothing}
                if (col != 0) {
                    try{
                    ans.add(new Coordinate(center.getRow() + row, center.getColumn()-col));
                    }
                    catch(IllegalArgumentException e){}//do nothing}
                }
            }
            colNum--;
        }
        return ans;
    }

    public boolean scanAction(TextPlayer enemy) throws IOException {
        int[] suqaresOccupied = { 0, 0, 0, 0 }; // represent Submarine/Destroyer/Battleship/Carrier in order
        Coordinate center = null;
        String prompt = "Please select which coordindate you want to scan, e.g. 'A0'";
        try {
            center = readCoordinate(prompt);
        } catch (IOException ioe) {
            out.println(ioe.getLocalizedMessage());
            System.exit(1);
        } catch(IllegalArgumentException ill){
            out.println("----------------------------------------------------------------");
            out.println(ill.getLocalizedMessage());
            out.println("Return to select your action again");
            out.println("----------------------------------------------------------------");
            return false;
        }
        // we scan the enemy board
        ArrayList<Coordinate> piecesToScan = scanPieces(center, 4, 4); // by default pattern
        for (Coordinate piece : piecesToScan) {
            Character mark = enemy.theBoard.whatIsAtForSelf(piece);
            if (mark != null) {
                if (mark.charValue() == 's') {
                    suqaresOccupied[0] += 1;
                } else if (mark.charValue() == 'd') {
                    suqaresOccupied[1] += 1;
                } else if (mark.charValue() == 'b') {
                    suqaresOccupied[2] += 1;
                } else {
                    suqaresOccupied[3] += 1;
                }
            }
        }
        out.println("----------------------------------------------------------------");
        out.println("Submarines occupy "+suqaresOccupied[0]+" squares");
        out.println("Destroyers occupy "+suqaresOccupied[1]+" squares");
        out.println("Battleships occupy "+suqaresOccupied[2]+" squares");
        out.println("Carriers occupy "+suqaresOccupied[3]+" squares");
        out.println("----------------------------------------------------------------");
        return true;
    }
    

}
