package ece651.sp22.xy142.battleship;

public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;

    public PlacementRuleChecker(PlacementRuleChecker<T> next){
        this.next = next;
    }
    /**
     * Subclass will override this method to specify how they check their own rule
     * @param theShip
     * @param theBoard
     * @return true/false
     */
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    public String checkPlacement(Ship<T> theShip, Board<T> theBoard){
        //if we fail our own rule: stop the placement
        String exceptionMsg = checkMyRule(theShip, theBoard);
        if(exceptionMsg!=null){
            return exceptionMsg;
        }
        //otherwise, ask the rest of the chain
        if(next!=null){
            return next.checkMyRule(theShip, theBoard);
        }
        return null;
    }
}
