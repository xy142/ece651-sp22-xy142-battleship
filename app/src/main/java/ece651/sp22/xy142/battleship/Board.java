package ece651.sp22.xy142.battleship;

public interface Board<T> {
  public int getWidth();

  public int getHeight();

  /**
   * try to add ship to the board; if can't place the ship, no throws only return
   * false
   * and the board keeps unchangeds
   * 
   * @param toAdd
   * @return true if place successfully
   */
  public String tryAddShip(Ship<T> toAdd);

  public T whatIsAtForSelf(Coordinate where);

  public T whatIsAtForEnemy(Coordinate where);
   
  /**
   * 
   * This method should search for any ship that occupies coordinate c
   * (you already have a method to check that)
   * If one is found, that Ship is "hit" by the attack and should
   * record it (you already have a method for that!). Then we
   * should return this ship.
   * 
   * If no ships are at this coordinate, we should record
   * the miss in the enemyMisses HashSet that we just made,
   * and return null.
   * 
   * @param c 
   * @return ship if hitted; otherwise null
   */
  public Ship<T> fireAt(Coordinate c);

  public boolean ifAllShipsSunk();


  /**
   * delete a ship form the board
   * @param toDel
   */
  public void deleteShip(Ship<T> toDel);


  /**
   * move the hits to new ship
   * @param oldShip
   * @param newShip
   */
  public void moveHits(Ship<T> oldShip, Ship<T> newShip);

  /**
   * select one ship by its part
   * @param part
   * @return null if no ship at that coordinate
   */
  public Ship<T> selectShipByPart(Coordinate part);
}
