package ece651.sp22.xy142.battleship;

public class NoCollisionRuleChecker<T>extends PlacementRuleChecker<T> {
    
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
    /**
     * check if two ships overlapping
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for(Coordinate c: theShip.getCoordinates()){
            if(theBoard.whatIsAtForSelf(c)!=null){
                return "That placement is invalid: the ship overlaps another ship.";
            }
        }
        return null;
    }
    
}
