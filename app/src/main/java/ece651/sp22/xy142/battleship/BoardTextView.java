package ece651.sp22.xy142.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of a Board(i.e., converting it to a string
 * to show to the user) It supports two ways to display the Board: one for the
 * player's own board, and one for the enemy's board
 */

public class BoardTextView {
  /**
   * The Board to display
   */
  private final Board<Character> toDisplay;

  /**
   * constructs a BoardView, given the board it will display
   * 
   * @param toDispaly is the Board to display
   * @throws IllegalArgumentException if the board is largre than 10*26
   */
  public BoardTextView(Board<Character> toDisplay) {
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
          "Board must be no longer than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }
    this.toDisplay = toDisplay;
  }

  /**
   * This makes the header line, e.g., 0|1|2|3|4\n
   *
   * @return the String that is the header line for the given board
   */
  String makeHeader() {
    StringBuilder ans = new StringBuilder("  "); // README shows two spaces at the beginning
    String sep = "";
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }
  /**
   * THIS IS A HELPER FUNCTION WE DO NOT CALL IT DIRECLTLY
   * CALL THE TWO FUNCTIONS BELOW DIRECTLY
   * Using lambda function here to make codes dry
   * we can call any function we want as input when calling this function
   * for display my board using (c)->toDislay.whatIsAtForSelf
   * for enemy using (c)->toDislay.whatIsAtForEnemy
   * @param getSquareFn
   * @return
   */
  protected String displayAnyOwnBoard(Function<Coordinate,Character> getSquareFn) {
    StringBuilder ans = new StringBuilder(this.makeHeader());
    for (int row = 0; row < toDisplay.getHeight(); row++) {
      ans.append((char) ('A' + row));
      String temp = "";
      for (int col = 0; col < toDisplay.getWidth(); col++) {
        ans.append(temp);
        if (getSquareFn.apply(new Coordinate(row, col)) != null) {
          ans.append(getSquareFn.apply(new Coordinate(row, col)));
        } else {
          ans.append(' ');
        }
        temp = "|";
      }
      ans.append((char) ('A' + row));
      ans.append('\n');
    }
    ans.append(this.makeHeader());
    return ans.toString();
  }

  public String displayMyOwnBoard(){
    return displayAnyOwnBoard((c)->toDisplay.whatIsAtForSelf(c));
  }
  
  public String displayEnemyBoard(){
    return displayAnyOwnBoard((c)->toDisplay.whatIsAtForEnemy(c));
  }

  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader,
  String enemyHeader){
    StringBuilder twoBoards = new StringBuilder();
    //1. combine the two headers into one line
    StringBuilder header = new StringBuilder();
    for (int i =0; i < 5;i ++){
      header.append(" ");
    }
    header.append(myHeader);

    for (int i =0; i< 3*toDisplay.getWidth();i++){
      header.append(" ");
    }
    header.append(enemyHeader+'\n');
    twoBoards.append(header);
    //2. split the board by \n
    String[] lines = this.displayMyOwnBoard().split("\n");
    String[] linesEne = enemyView.displayEnemyBoard().split("\n");
    //3. combine
    for(int i =0; i< lines.length;i++){
      StringBuilder newLine = new StringBuilder();
      newLine.append(lines[i]);
      for(int j =0; j<2*toDisplay.getWidth();j++){
        newLine.append(" ");
      }
      newLine.append(linesEne[i]);
      twoBoards.append(newLine+"\n");
    }
    return twoBoards.toString();
  }

  public String displayMyBoardWithEnemyNextToIt(TextPlayer enemy){
    return displayMyBoardWithEnemyNextToIt(enemy.view, "Your ocean", "Player "+enemy.name+"'s ocean");
  }

}
